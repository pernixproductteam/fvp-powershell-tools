# Get ESXi vib Information
# Author: Andy Daniel
# Created: 06/09/2015
# Last Updated: v0.1 - 06/09/2015 - Andy Daniel
# v0.1 Update - Initial creation.
#
# Notes: This script will use esxcli to find FVP vib versions installed on hosts and output to a grid-view.
#        This version also finds Proximal AutoCache vib versions for removal. :)                    
# Usage: Connect to your vCenter instance with "connect-viserver" and run this script.
#
$report = @()
$esxclusters = get-cluster
foreach ($esxcluster in $esxclusters) {
	Write-Progress -id 1 -activity "Scanning Clusters" -status "Scanning cluster $($esxcluster.Name)" -PercentComplete $(($i/$esxclusters.count)*100)
	$esxhosts = $esxcluster | get-vmhost
	$j=0
	foreach ($esxhost in $esxhosts) {
		Write-Progress -id 2 -activity "Scanning Hosts" -status "Scanning host $($esxhost.Name)" -PercentComplete $(($j/$esxhosts.count)*100)
		$row = "" | Select Cluster,Host,PernixData,Proximal
		$row.Cluster = $esxcluster.Name
		$row.Host = $esxhost.Name
		$esxcli = Get-EsxCli -VMHost $esxhost
		$vibs = $esxcli.software.vib.list()
		foreach ($vib in $vibs) {
			if ($vib.Name -match "pernixcore") {
				$row.PernixData = $vib.Version
				}
			if ($vib.Name -match "proximal") {
				$row.Proximal = $vib.Version
				}
			if ($vib.Name -eq "pdi-cache") {
				$row.Proximal = $vib.Version
				}
		}
		$report += $row
		$j++
	}
	$i++
}
$report | sort Cluster,Host | Out-GridView