# Set PSP Tuning Claim Rules
# Author: Andy Daniel
# Created: 06/09/2015
#
# Notes: This script will set claim rules for several array vendors and check for current
#		 optimization. It will remove existing conflicting rules and reboot hosts as necessary.
#                             
# Usage: Provide -VMHost and -LogPath params if necessary. Otherwise modify line 26 to specify custom set of hosts.
#		 Hosts must be in maintenance.
#
Param(
  [string]$VMHost,
  [string]$LogPath
)
$VMHosts = ""

if ($VMHost) { 
  $VMHosts = Get-VMHost $VMHost
}

If ($LogPath -eq ""){
	$LogPath = Convert-Path .
}

$logfile = $LogPath + "\set-psp-tuning-claim-rules_$($VMHost)_$(get-date -f yyyy_MM_dd_HH_mm_ss).log"

do {$continue = Read-Host "This script will stop prnxd and may immediately reboot hosts if required. Press y to continue or e to exit."} until (($continue.ToLower() -eq "y") -or ($continue.ToLower() -eq "e"))
if ($continue.ToLower() -eq "e") {Write-Host "You chose to exit. - Exiting" -fo yellow; break;}

$root_pass = Get-Credential -UserName root -Message "Enter ESXi host root password:"
$myDir = Convert-Path .
$plinkLocation = $myDir + "\plink.exe"

If (-not (Test-Path $plinkLocation)){
   Write-Host "plink.exe not found, trying to download..." -fo yellow
   "plink.exe not found, trying to download..." | Out-File  $logfile -append
   $WC = new-object net.webclient
   $WC.DownloadFile("http://the.earth.li/~sgtatham/putty/latest/x86/plink.exe",$plinkLocation)
   If (-not (Test-Path $plinkLocation)){
      Write-Host "Unable to download plink.exe, please download from the following URL and add it to the same folder as this script: http://the.earth.li/~sgtatham/putty/latest/x86/plink.exe" -fo Red
      "Unable to download plink.exe" | Out-File  $logfile -append
	  Exit
   } Else {
      $plink = Get-ChildItem $plinkLocation
      If ($plink.length -gt 0) {
         Write-Host "plink.exe downloaded, continuing..."
		 "plink.exe downloaded." | Out-File  $logfile -append
      } Else {
         Write-Host "Unable to download plink.exe, please download from the following URL and add it to the same folder as this script: http://the.earth.li/~sgtatham/putty/latest/x86/plink.exe" -fo Red
         "Unable to download plink.exe" | Out-File  $logfile -append
		 Exit
      }
   }  
}

$VMHosts | where {$_.ConnectionState -eq "Maintenance"} | ForEach-Object {
$enabled_ssh = $false
$ssh_service = $_ | Get-VMHostService | Where {$_.Key -eq "TSM-SSH"}
	if (-not $ssh_service.Running){
		$enabled_ssh = $true
		$ssh_service | Start-VMHostService
	}

Write-Host "Stopping prnxd..."
"Stopping prnxd..." | Out-File  $logfile -append

echo y | cmd /c "plink.exe -ssh -pw $($root_pass.GetNetworkCredential().password) root@$($_.Name) vmware -v" | Out-Null
$prnxdstop = cmd /c "plink.exe -ssh -pw $($root_pass.GetNetworkCredential().password) root@$($_.Name) /etc/init.d/prnxd stop"
$prnxdstop | Out-File  $logfile -append
If ($prnxdstop -notcontains "done"){
		Write-Host "Unable to stop prnxd." -fo Red
		Write-Host $prnxdstop -fo Red
		"Unable to stop prnxd." | Out-File  $logfile -append
		Exit
	}

$esxcli = Get-EsxCli -VMHost $_
Write-Host "Adding claim rules..."
"Adding claim rules..." | Out-File  $logfile -append
$claimrules = @()
$claimrules += (,("VMW_SATP_EQL","EQLOGIC","100E-00","VMW_PSP_RR","iops=3","$null","FVP EQL Optimized; EQL TR1091"))
$claimrules += (,("VMW_SATP_DEFAULT_AA","3PARdata","VV","VMW_PSP_RR","iops=1","tpgs_off","FVP 3Par A/A Optimized; HP QL226-97872"))
$claimrules += (,("VMW_SATP_ALUA","3PARdata","VV","VMW_PSP_RR","iops=1","tpgs_on","FVP 3Par ALUA Optimized; HP QL226-97872"))
$claimrules += (,("VMW_SATP_ALUA","Nimble","Server","VMW_PSP_RR","iops=1","$null","FVP Nimble ALUA Optimized; Nimble KB-000059"))
$claimrules += (,("VMW_SATP_SYMM","EMC","SYMMETRIX","VMW_PSP_RR","iops=1","$null","FVP VMAX ALUA Optimized; EMC H2529.13"))
$claimrules += (,("VMW_SATP_EVA","HP","HSV*","VMW_PSP_RR","iops=1","tpgs_off","FVP EVA A/A Optimized; HP 4AA1-2185ENW"))
$claimrules += (,("VMW_SATP_ALUA","HP","HSV*","VMW_PSP_RR","iops=1","tpgs_on","FVP EVA ALUA Optimized; HP 4AA1-2185ENW"))
$claimrules += (,("VMW_SATP_DEFAULT_AA","XtremIO","XtremApp","VMW_PSP_RR","iops=1","tpgs_off","FVP XtremIO A/A RR Optimized; EMC 302-001-288"))
$claimrules += (,("VMW_SATP_ALUA","PURE","FlashArray","VMW_PSP_RR","iops=1","$null","FVP Pure ALUA Optimized; Pure vSphere BPG"))
$claimrules += (,("VMW_SATP_ALUA","Skyera","Skyhawk","VMW_PSP_RR","iops=1","$null","FVP Skyera Skyhawk ALUA RR Optimized"))
$claimrules += (,("VMW_SATP_ALUA","NETAPP","$null","VMW_PSP_RR","$null","tpgs_on","FVP NetApp ALUA"))
$claimrules += (,("VMW_SATP_ALUA","TEGILE","ZEBI-FC","VMW_PSP_RR","$null","tpgs_on","FVP Tegile FC ALUA; Tegile TCS"))
$claimrules += (,("VMW_SATP_DEFAULT_AA","COMPELNT","Compellent Vol","VMW_PSP_RR","iops=3","$null","FVP Compellent Optimized; Dell 680-041-020"))
$reboot_required = $false
foreach ($claimrule in $claimrules){
	try {
		$esxcli.storage.nmp.satp.rule.add($null,$claimrule[5],$claimrule[6],$null,$null,$true,$claimrule[2],$null,$claimrule[3],$claimrule[4],$claimrule[0],$null,$null,$claimrule[1]) | Out-Null
		if ($esxcli.storage.nmp.device.list() | Where {$_.StorageArrayType -eq $claimrule[0] -and $_.PathSelectionPolicyDeviceConfig -notmatch $claimrule[4] -and $_.PathSelectionPolicyDeviceCustomConfig -notmatch $claimrule[4]}) {
				$reboot_required = $true
				Write-Host "$($claimrule[1]) $($claimrule[2]) rule added, but unoptimized config found, reboot required." -fo yellow
				"$($claimrule[1]) $($claimrule[2]) rule added, but unoptimized config found, reboot required." | Out-File  $logfile -append
			}
			else {
				"$($claimrule[1]) $($claimrule[2]) rule added." | Out-File  $logfile -append
			}
	}
	catch {
		if ($_.Exception.Message -match "Duplicate user rule found" -or $_.Exception.Message -match "Bad parameter"){
			try {
				$rule_to_delete = $esxcli.storage.nmp.satp.rule.list() | where {$_.ClaimOptions -eq $claimrule[5] -and $_.Model -eq $claimrule[2] -and $_.Name -eq $claimrule[0] -and $_.Vendor -eq $claimrule[1] -and $_.RuleGroup -eq "user"}
				$esxcli.storage.nmp.satp.rule.remove($null,$rule_to_delete.ClaimOptions,$rule_to_delete.Description,$null,$null,$rule_to_delete.Model,$null,$rule_to_delete.DefaultPSP,$rule_to_delete.PSPOptions,$rule_to_delete.Name,$null,$null,$rule_to_delete.Vendor) | Out-Null
				Write-Host "$($claimrule[1]) $($claimrule[2]) rule already present. It was removed!" -fo yellow
				"$($claimrule[1]) $($claimrule[2]) rule already present. It was removed!" | Out-File  $logfile -append
			}
			catch {
				Write-Host "Error removing existing $($claimrule[1]) $($claimrule[2]) rule: $_.Exception.Message" -fo red
				"Error removing existing $($claimrule[1]) $($claimrule[2]) rule: $_.Exception.Message" | Out-File  $logfile -append
			}
			try {
				$esxcli.storage.nmp.satp.rule.add($null,$claimrule[5],$claimrule[6],$null,$null,$true,$claimrule[2],$null,$claimrule[3],$claimrule[4],$claimrule[0],$null,$null,$claimrule[1]) | Out-Null
				if ($esxcli.storage.nmp.device.list() | Where {$_.StorageArrayType -eq $claimrule[0] -and $_.PathSelectionPolicyDeviceConfig -notmatch $claimrule[4] -and $_.PathSelectionPolicyDeviceCustomConfig -notmatch $claimrule[4]}) {
						$reboot_required = $true
						Write-Host "$($claimrule[1]) $($claimrule[2]) rule added, but unoptimized config found, reboot required." -fo yellow
						"$($claimrule[1]) $($claimrule[2]) rule added, but unoptimized config found, reboot required." | Out-File  $logfile -append
					}
					else {
						Write-Host "$($claimrule[1]) $($claimrule[2]) rule added."
						"$($claimrule[1]) $($claimrule[2]) rule added." | Out-File  $logfile -append
					}
			}
			catch {
					Write-Host "Error adding $($claimrule[1]) $($claimrule[2]) rule: $_.Exception.Message" -fo red
					"Error adding $($claimrule[1]) $($claimrule[2]) rule: $_.Exception.Message" | Out-File  $logfile -append
				  }
		}
		else {
			Write-Host "Error adding $($claimrule[1]) $($claimrule[2]) rule: $_.Exception.Message" -fo red
			"Error adding $($claimrule[1]) $($claimrule[2]) rule: $_.Exception.Message" | Out-File  $logfile -append
			}
	}
}

Write-Host "Clearing LUN specific PSP configuration..."
"Clearing LUN specific PSP configuration..." | Out-File $logfile -append
"POST PSP CONFIG" | Out-File $logfile -append
$esxcli.storage.nmp.device.list() | Out-File  $logfile -append
$esxcli.storage.nmp.device.list() | Where {($_.StorageArrayType -ne "VMW_SATP_LOCAL") -and ($_.PathSelectionPolicy -like "PRNX*")} | group-object Device | %{
	$esxcli.storage.nmp.device.list($_.Name) | Out-File  $logfile -append
	$esxcli.storage.nmp.device.set($true, $_.Name, $null) | Out-File  $logfile -append
	$esxcli.storage.nmp.psp.generic.deviceconfig.set($false, "", $_.Name) | Out-File  $logfile -append
	}
"POST PSP CONFIG" | Out-File $logfile -append
$esxcli.storage.nmp.device.list() | Out-File $logfile -append
if($reboot_required){
	Write-Host "Rebooting the host..."
	"Rebooting the host..." | Out-File $logfile -append
	Restart-VMHost $_ -confirm:$false | Out-Null
} else {		
	Write-Host "Starting prnxd..."
	"Starting prnxd..." | Out-File  $logfile -append
	$prnxdstart = cmd /c "plink.exe -ssh -pw $($root_pass.GetNetworkCredential().password) root@$($_.Name) /etc/init.d/prnxd start"
	Write-Host $prnxdstart
	$prnxdstart | Out-File $logfile -append
	if($enabled_ssh){
		$ssh_service | Stop-VMHostService	
	}
}
}


