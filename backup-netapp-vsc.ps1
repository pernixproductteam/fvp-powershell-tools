# NetApp VSC Backup Write Back to Write Through Transition
# Author: Andy Daniel
# Created: 07/23/2014
# Last Updated: v1.2 - 11/18/2016 - Andy Daniel
# v1.2 Update - Set VM policy back to WB doesn't require WaitTimeSecs.
# v1.1 Update - Major rewrite to handle VMs with datastore policy and WB external peers. New password
#               encryption method doesn't require creating password as VSC user.
# v0.6 Update - Changed VM writeback peers environment variable to be unique to support
#               running multiple backups simultaneously.
#
# Notes: This script will transition VMs being backed up by VSC into WT mode before
#        NetApp snapshot and transition them back to WB mode when done.
#                             
# Usage: Call this script with VSC using a wrapper file named vsc_backup.cmd that contains the following:
#        powershell "& '<full path to ps1 script>\vsc_backup.ps1'
#       
#		 Use the Powershell line below to create the $passwordfile credential to be used later. NOTE
#		 THAT THE KEY TO THIS FILE IS CONTAINED IN THE SCRIPT AND CAN BE EASILY DECRYPTED BY USERS
#		 WITH ACCESS TO BOTH THE SCRIPT AND THE PASSWORD FILE. IT IS RECOMMENDED TO RUN THE VSC SERVICE
#		 AS A NAMED USER AND USE NTFS ACCESS CONTROLS TO LIMIT ACCESS TO THE FILES:
#		 Read-Host -AsSecureString -prompt "Enter password" | ConvertFrom-SecureString -key $([Byte[]](1..16))| Out-File password.txt 
#

#Set these required parameters
$transition_timeout = 300 #This is the time that the script will wait for a VM to transitiont from Write Back to Write Through before erroring out.
$logfilepath = "E:\Program Files\NetApp\Virtual Storage Console\smvi\server\scripts\log" #Path to log files.
$passwordfile = "E:\Program Files\NetApp\Virtual Storage Console\smvi\server\etc\vsc_fvp_enc_pass.txt" #Path to the encrypted password file (see Usage section above to create).
$fvp_server = "prnxms01.domain.local" #PernixData Management Server name or IP address.
$fvp_username = "<domain>\<username>" #Username with Administrative rights to vCenter/FVP.


Function WriteLog
{
   Param ([string]$logstring)
   $logstring = "$(Get-Date -format s) - " + $logstring
   $logstring | out-file -Filepath $logfile -append
}

Function transition_wb_vm ([string]$vm, [string]$wb_type, [int]$destage_timeout) {
	if(!$destage_timeout){
		$destage_timeout = 600
	}
	$prnx_vm = Get-PrnxObject "$vm" -ea Stop
	$cluster_uuid = $prnx_vm.wbState.cache[0]
	if ($prnx_vm.stats.destager.timeToDestageUS -gt 0){
		$destage_est = [int][Math]::Ceiling($prnx_vm.stats.destager.timeToDestageUS/1000000)	
	} else {
		$destage_est = 1
	}
	WriteLog "Estimated destage time for $vm is $destage_est seconds."
	if($destage_est -le $destage_timeout){
		$total_sleep = 0
		if ($wb_type -eq "datastore WB"){
			WriteLog "Adding VM specific Write Through policy for $vm to override datastore policy."
			Add-PrnxVirtualMachineToFVPCluster -FVPCluster $cluster_uuid -Name "$vm" -WriteThrough -ea Stop
		} else {
			Set-PrnxAccelerationPolicy -Name "$vm" -WriteThrough -ea Stop
		}
		while (($prnx_vm.wbInfo.wbState -gt 0) -and ($total_sleep -lt $destage_timeout)){
			$prnx_vm = Get-PrnxObject "$vm" -ea Stop
			$destage_est = [int][Math]::Ceiling($prnx_vm.stats.destager.timeToDestageUS/1000000)
			$total_sleep = $total_sleep + $destage_est
			WriteLog "Waiting $destage_est seconds for $vm to destage."
			start-sleep -s $destage_est
		}
		if ($prnx_vm.wbInfo.wbState -gt 0){
			Throw "Total estimated destage time for $vm is $destage_est seconds. This is greater than configured transition timeout of $destage_timeout seconds."
		} else {
			WriteLog "$vm successfully transitioned to Write Through."
		}
	} else {
		Throw "Estimated destage time for $vm is $destage_est seconds. This is greater than configured transition timeout of $destage_timeout seconds."
	}
}

#Initialize Variables
$backup_name = [Environment]::GetEnvironmentVariable("BACKUP_NAME","Process")
$backup_date = [Environment]::GetEnvironmentVariable("BACKUP_DATE","Process")
$backup_time = [Environment]::GetEnvironmentVariable("BACKUP_TIME","Process")
$backup_phase = [Environment]::GetEnvironmentVariable("BACKUP_PHASE","Process")
$logfile = $logfilepath + "\vsc_fvp_$(get-date -f yyyy_MM_dd_HH_mm_ss)_$backup_phase.log"
[int]$vm_count = 0
$vm_count = [Environment]::GetEnvironmentVariable("VIRTUAL_MACHINES","Process")
$error_msg = ""
$vm_array = @()

Try { 
		WriteLog "Beginning script in $backup_phase phase"
		WriteLog "Retrieving encrypted password from file $passwordfile"
	}
Catch { 
		WriteLog "Error writing to log file."
		Exit 1
	}

Try { 
		[Byte[]] $key = (1..16)
		$fvp_enc_pass = Get-Content $passwordfile | ConvertTo-SecureString -Key $key
	}
Catch { 
		WriteLog "Error retrieving encrypted password"
		Exit 1
	}

Try { 
		$credential = New-Object System.Management.Automation.PsCredential($fvp_username, $fvp_enc_pass)
	}
Catch {
		WriteLog "Error creating credential object"
		Exit 1
	}
WriteLog "Connecting to FVP Management Server: $fvp_server"
Try {
		import-module prnxcli -ea Stop
		Connect-PrnxServer $fvp_server -Credentials $credential -ea Stop
		$prnxms = Get-PrnxObject -Type PrnxMServer
        if($prnxms.licensedFeatures -match "FVP_FEATURE_FAULT_DOMAIN"){
			$fault_domains_licensed = $true
			WriteLog "Connected to $fvp_server, version $($prnxms.buildinfo), Enterprise License"
		} else {
			WriteLog "Connected to $fvp_server, version $($prnxms.buildinfo), Standard License"
		}
	}
Catch {
		WriteLog "Error connecting to FVP Management Server: $($_.Exception.Message)"
		Exit 1
    }
If ($vm_count -eq 0) {
		WriteLog "Unable to import VM(s) from VSC"
		Exit 1
}
WriteLog "Importing VM information passed from VSC"
for ($i=1; $i -le $vm_count; $i++) {
		$elements = [Environment]::GetEnvironmentVariable("VIRTUAL_MACHINE.$i","process").split("|")
		$vm_array += ,$elements
		WriteLog "Imported VM: $([Environment]::GetEnvironmentVariable("VIRTUAL_MACHINE.$i","process"))"
	}
WriteLog "Imported $vm_count VM(s) from VSC"
$vm_index=1
if ($backup_phase -eq "PRE_BACKUP") {
	foreach ($vm in $vm_array) {
		Try {
				$prnx_vm = Get-PrnxObject $vm[0] -ea Stop
		}
		Catch {
			WriteLog "Unable to find $($vm[0]) in FVP Inventory: $($_.Exception.Message)"
			$error_msg = "Unable to find $($vm[0]) in FVP Inventory: $($_.Exception.Message)"
			Exit 1
		}
		if ($prnx_vm.wbInfo.wbState -eq 1) {
			if ($vm[2] -eq "POWERED_ON") {
				$cluster_uuid = $prnx_vm.wbState.cache[0]
				$wb_peers = $prnx_vm.policy.numWBPeers
				if($fault_domains_licensed){
					$wb_ext_peers = $prnx_vm.policy.numWBExternalPeers
				}else{
					$wb_ext_peers = 0
				}
				$wb_type = $prnx_vm.policyStatus 
				WriteLog "Transitioning $($vm[0]): Currently Powered On in Writeback with $wb_peers peers, $wb_ext_peers external"
				Try {
					[Environment]::SetEnvironmentVariable("VIRTUAL_MACHINE.$vm[0].peers", $wb_peers, "user")
					[Environment]::SetEnvironmentVariable("VIRTUAL_MACHINE.$vm[0].ext_peers", $wb_ext_peers, "user")
					[Environment]::SetEnvironmentVariable("VIRTUAL_MACHINE.$vm[0].wb_type", $wb_type, "user")
				}
				Catch {
					WriteLog "Failed to create environment variable to store write back peer count: $($_.Exception.Message)"
					$error_msg = "Failed to create environment variable to store write back peer count: $($_.Exception.Message)"
					Exit 1
				}
				Try { 
					transition_wb_vm $vm[0] $wb_type $transition_timeout
				}
				Catch {
					WriteLog "Failed to transition $($vm[0]): $($_.Exception.Message)"
					$error_msg = "Transition Error: Failed to transition $($vm[0]): $($_.Exception.Message)"
					Exit 1
				}
			} else {
				WriteLog "Skipping $($vm[0]): Not Powered On"
			}
		} else {
			Try {
				[Environment]::SetEnvironmentVariable("VIRTUAL_MACHINE.$vm[0].peers", "WT", "user")
				[Environment]::SetEnvironmentVariable("VIRTUAL_MACHINE.$vm[0].ext_peers", "WT", "user")
				}
				Catch {
					WriteLog "Failed to create environment variable to store write back peer count: $($_.Exception.Message)"
					$error_msg = "Failed to create environment variable to store write back peer count: $($_.Exception.Message)"
					Exit 1
				}
			WriteLog "Skipping $($vm[0]): Not in Writeback"
		}
	$vm_index++
	}
} elseif (($backup_phase -eq "POST_BACKUP") -or ($backup_phase -eq "FAILED_BACKUP")) {
	foreach ($vm in $vm_array) {
		$wb_peers = $null
		$wb_ext_peers = $null
		$wb_type = $null
		Try {
			$prnx_vm = Get-PrnxObject $vm[0] -ea Stop
		}
		Catch {
			WriteLog "Unable to find $($vm[0]) in FVP Inventory: $($_.Exception.Message)"
			$error_msg = "Unable to find $($vm[0]) in FVP Inventory: $($_.Exception.Message)"
			continue
		}
		$wb_peers = [Environment]::GetEnvironmentVariable("VIRTUAL_MACHINE.$vm[0].peers", "user")
		$wb_ext_peers = [Environment]::GetEnvironmentVariable("VIRTUAL_MACHINE.$vm[0].ext_peers", "user")
		$wb_type = [Environment]::GetEnvironmentVariable("VIRTUAL_MACHINE.$vm[0].wb_type", "user")
		[Environment]::SetEnvironmentVariable("VIRTUAL_MACHINE.$vm[0].peers",$null,"user")
		[Environment]::SetEnvironmentVariable("VIRTUAL_MACHINE.$vm[0].ext_peers",$null,"user")
		[Environment]::SetEnvironmentVariable("VIRTUAL_MACHINE.$vm[0].wb_type",$null,"user")
		if (($wb_peers -eq $null) -or ($wb_ext_peers -eq $null)) {
			WriteLog "Skipping $($vm[0]): Unable to read previous Writeback peer count"
			$error_msg = "Skipping $($vm[0]): Unable to read previous Writeback peer count"
		} elseif ($wb_peers -eq "WT") {
			WriteLog "Skipping $($vm[0]): Not previously in Writeback"
		} elseif (($prnx_vm.wbInfo.wbState -eq 1) -and ($prnx_vm.policy.numWBPeers -eq $wb_peers) -and ($prnx_vm.policy.numWBExternalPeers -eq $wb_ext_peers)) {
			WriteLog "Skipping $($vm[0]): Already in Writeback with previous peer count"
		} elseif (($prnx_vm.wbInfo.wbState -ne 1) -or ($prnx_vm.policy.numWBPeers -ne $wb_peers) -or ($prnx_vm.policy.numWBExternalPeers -ne $wb_ext_peers)){
			WriteLog "Transitioning $($vm[0]): Previously in Writeback with $wb_peers peers, $wb_ext_peers external peers."
			Try {
				if ($wb_type -eq "datastore WB"){
					WriteLog "Removing VM specific policy for $($vm[0]). VM will re-inherit datastore policy."
					Remove-PrnxObjectFromFVPCluster -Object $prnx_vm -FVPCluster $prnx_vm.wbState.cache[0] -ea Stop
				} else {
					WriteLog "Setting VM specific policy for $($vm[0]) VM to Write Back."
					if($fault_domains_licensed){
						Set-PrnxAccelerationPolicy -Object $prnx_vm -WriteBack -NumWBPeers $wb_peers -NumWBExternalPeers $wb_ext_peers -ea Stop
					} else {
						Set-PrnxAccelerationPolicy -Object $prnx_vm -WriteBack -NumWBPeers $wb_peers -ea Stop
					}
				}
			}
			Catch {
				WriteLog "Failed to transition $($vm[0]): $($_.Exception.Message)"
				$error_msg = "Failed to transition $($vm[0]): $($_.Exception.Message)"
				continue
			}
		}                              
	$vm_index++
	}
} 
WriteLog "Disconnecting from FVP Management Server: $fvp_server"
Disconnect-PrnxServer > $null
if ($error_msg -eq "") {
    WriteLog "Script completed without error"
    exit 0
} else {
    WriteLog "Script completed with error(s):"
    WriteLog $error_msg
    exit 1
}