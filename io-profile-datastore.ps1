# Datastore Intelligent I/O Profiling
# Author: Andy Daniel
# Created: 12/16/2016
# Last Updated: v0.1 - 12/16/2016 - Andy Daniel
# v0.1 Initial release.
#
# Notes: This script will enable Intelligent I/O Profiling to suspend data population for VMs being backed up 
#        on a datastore and transition them back when done.
#                             
# Usage: Install or copy the PernixData Powershell module and VMware PowerCLI onto the server executing the backup and create an encrypted 
#		 password file (see insructions below).
#		 You may also need to set the Powershell execution policy to "unrestricted".
#		 Set the required parameters in the script below and then copy to the server.
#        Call powershell.exe using EXACT SYNTAX (change script path) below and provide the -Datastore and -Phase parameters as appropriate:
#
#		 C:\Windows\System32\WindowsPowerShell\v1.0\powershell.exe "& 'C:\io-profile-datastore.ps1' "-Datastore datastore01 -Phase pre"
#
#		 Use the Powershell line below to create the $passwordfile credential to be used later. NOTE
#		 THAT THE KEY TO THIS FILE IS CONTAINED IN THE SCRIPT AND CAN BE EASILY DECRYPTED BY USERS
#		 WITH ACCESS TO BOTH THE SCRIPT AND THE PASSWORD FILE. IT IS RECOMMENDED TO 
#		 USE NTFS ACCESS CONTROLS TO LIMIT ACCESS TO THE FILES:
#		 Read-Host -AsSecureString -prompt "Enter password" | ConvertFrom-SecureString -key $([Byte[]](1..16))| Out-File fvp_enc_pass.txt 
#
Param(
  [string]$Phase, #This is the phase of the backup, either "pre" or "post".
  [string]$Datastore #This is the name of the datastore as it appears in vCenter.
)

#Set these required parameters
$transition_timeout = 300 #This is the time that the script will wait for a VM to transition before erroring out.
$logfilepath = "C:\log" #Path to log files.
$passwordfile = "C:\fvp_enc_pass.txt" #Path to the encrypted password file (see Usage section above to create).
$fvp_server = "pernixms01.pernixdata.com" #PernixData Management Server name or IP address.
$vcenter_server = "vcenter.domain.local" #vCenter Server name or IP address.
$username = "domain\svc_account" #Username with Administrative rights to vCenter/FVP.

#Initialize Variables
$logfile = $logfilepath + "\io_profile_datastore_$(get-date -f yyyy_MM_dd_HH_mm_ss)_$phase.log"
[int]$vm_count = 0
$error_msg = ""
$vm_array = @()

Function WriteLog
{
   Param ([string]$logstring)
   $logstring = "$(Get-Date -format s) - " + $logstring
   $logstring | out-file -Filepath $logfile -append
}

Function transition_vm ([string]$vm, [int]$timeout) {
	if(!$timeout){
		$timeout = 600
	}
	$total_sleep = 0
	$prnx_vm = Get-PrnxObject "$vm" -Type VM -ea Stop
	Suspend-PrnxReadWriteDataPopulation -ObjectIdentifier $prnx_vm -ea Stop
	while ((($prnx_vm.effectivePolicy -ne 531) -and ($prnx_vm.effectivePolicy -ne 535)) -and ($total_sleep -lt $timeout)){
		$prnx_vm = Get-PrnxObject "$vm" -ea Stop
		$total_sleep = $total_sleep + 5
		WriteLog "Waiting 5 seconds for $vm to transition."
		start-sleep -s 5
	}
	if (($prnx_vm.effectivePolicy -ne 531) -and ($prnx_vm.effectivePolicy -ne 535)){
		Throw "$vm did not transition within timeout of $timeout seconds."
	} else {
		WriteLog "Intelligent I/O Filtering successfully enabled for $vm."
	}
}

WriteLog "Beginning script in $phase phase"

Try { 
		WriteLog "Retrieving encrypted password from file $passwordfile"
		[Byte[]] $key = (1..16)
        $fvp_enc_pass = Get-Content $passwordfile | ConvertTo-SecureString -Key $key
	}
Catch { 
		WriteLog "Error retrieving encrypted password"
		Exit 1
	}

Try { 
		$credential = New-Object System.Management.Automation.PsCredential($username, $fvp_enc_pass)
	}
Catch {
		WriteLog "Error creating credential object"
		Exit 1
	}

Try {
		WriteLog "Connecting to vCenter Server: $vcenter_server"
		Add-PSSnapin VMware.VimAutomation.Core -ErrorAction Stop
		$vmware = Connect-VIServer -Server $vcenter_server -credential $credential
		WriteLog "Successfully connected to vCenter Server: $($vmware.Name)"
		Writelog "Building list of VMs in $Datastore"
		$DatastoreVMs = Get-VM -Datastore $Datastore
		foreach ($DatastoreVM in $DatastoreVMs){
			$vm_array += ,$($DatastoreVM.Name, $DatastoreVM.PersistentId, $DatastoreVM.PowerState)
		}
		$vm_count = $vm_array.Count
}
Catch {
		WriteLog "Error retrieving VMs in backup job: $($_.Exception.Message)"
		Exit 1
}

If ($vm_count -eq 0) {
		WriteLog "No powered on VM(s) imported from datastore!"
		Exit 1
}

WriteLog "Imported $vm_count VM(s) from Datastore"
WriteLog "Connecting to FVP Management Server: $fvp_server"
Try {
		import-module prnxcli -ea Stop
		Connect-PrnxServer $fvp_server -Credentials $credential -ea Stop
		$prnxms = Get-PrnxObject -Type PrnxMServer
        if($prnxms.licensedFeatures -match "FVP_FEATURE_FAULT_DOMAIN"){
			$fault_domains_licensed = $true
			WriteLog "Connected to $fvp_server, version $($prnxms.buildinfo), Enterprise License"
		} else {
			WriteLog "Connected to $fvp_server, version $($prnxms.buildinfo), Standard License"
		}
	}
Catch {
		WriteLog "Error connecting to FVP Management Server: $($_.Exception.Message)"
		Exit 1
    }

$vm_index=1
if ($phase -eq "pre") {
	foreach ($vm in $vm_array) {
		Try {
				$prnx_vm = Get-PrnxObject $vm[0] -Type VM -ea Stop
		}
		Catch {
			WriteLog "Unable to find $($vm[0]) in FVP Inventory: $($_.Exception.Message)"
			WriteLog "Disconnecting from FVP Management Server: $fvp_server"
			Disconnect-PrnxServer > $null
			WriteLog "Disconnecting from vCenter Server: $vcenter_server"
			Disconnect-VIServer -Confirm:$false -server $vcenter_server > $null
			Exit 1
		}
		if ($vm[2] -eq "PoweredOn" -and $prnx_vm.effectivePolicy -ne 1 -and $prnx_vm.effectivePolicy -ne 65) {
			if (($prnx_vm.effectivePolicy -ne 531) -and ($prnx_vm.effectivePolicy -ne 535)) {
					WriteLog "Enabling Intelligent I/O Filtering for $($vm[0])."
					Try {
						[Environment]::SetEnvironmentVariable("VIRTUAL_MACHINE.$vm[1].cachePolicy", $prnx_vm.cachePolicy, "user")
					}
					Catch {
						WriteLog "Failed to create environment variable to store cache policy: $($_.Exception.Message)"
						$error_msg = "Failed to create environment variable to store cache policy: $($_.Exception.Message)"
						Exit 1
					}
					Try { 
						transition_vm $vm[0] $transition_timeout
					}
					Catch {
						WriteLog "Failed to enable Intelligent I/O Filtering for $($vm[0]) : $($_.Exception.Message)"
						WriteLog "Disconnecting from vCenter Server: $vcenter_server"
						Disconnect-VIServer -Confirm:$false -server $vcenter_server > $null
						WriteLog "Disconnecting from FVP Management Server: $fvp_server"
						Disconnect-PrnxServer > $null
						Exit 1
					}
			} else {
				WriteLog "Skipping $($vm[0]) : Intelligent I/O Filtering already enabled."
			}
		} else {
				WriteLog "Skipping $($vm[0]): Not powered on and accelerated."
		}
	$vm_index++
	}
} elseif ($phase -eq "post"){
	foreach ($vm in $vm_array) {
		$vm_policy = $null
		Try {
			$prnx_vm = Get-PrnxObject $vm[0] -Type VM -ea Stop
		}
		Catch {
			WriteLog "Unable to find $($vm[0]) in FVP Inventory: $($_.Exception.Message)"
			$error_msg = "Unable to find $($vm[0]) in FVP Inventory: $($_.Exception.Message)"
			continue
		}
	if ($vm[2] -eq "PoweredOn" -and $prnx_vm.effectivePolicy -ne 1 -and $prnx_vm.effectivePolicy -ne 65) {
		$vm_policy = [Environment]::GetEnvironmentVariable("VIRTUAL_MACHINE.$vm[1].cachePolicy", "user")
		if ($vm_policy -eq $null) {
				WriteLog "Skipping $($vm[0]): Unable to read previous policy"
				$error_msg = "Skipping $($vm[0]): Unable to read previous policy"
			} else {
				[Environment]::SetEnvironmentVariable("VIRTUAL_MACHINE.$vm[1].cachePolicy",$null,"user")
				Try {
					WriteLog "Disabling Intelligent I/O Filtering for $($vm[0]) to resume data population."
					Resume-PrnxReadWriteDataPopulation -ObjectIdentifier $prnx_vm -ea Stop
					}
				Catch {
						WriteLog "Unable to disable Intelligent I/O Filtering for $($vm[0]) and resume data population: $($_.Exception.Message)"
						$error_msg = "Unable to disable Intelligent I/O Filtering for $($vm[0]) and resume data population: $($_.Exception.Message)"
						continue
					}
			}
		}else {
				WriteLog "Skipping $($vm[0]): Not powered on and accelerated."
		}
	$vm_index++
	}
} else {
		WriteLog "No valid backup phase provided. Backup phase of 'pre' or 'post' should be provided via -Phase parameter."
		WriteLog "Disconnecting from vCenter Server: $vcenter_server"
		Disconnect-VIServer -Confirm:$false -server $vcenter_server > $null
		WriteLog "Disconnecting from FVP Management Server: $fvp_server"
		Disconnect-PrnxServer > $null
		Exit 1
}

WriteLog "Disconnecting from vCenter Server: $vcenter_server"
Disconnect-VIServer -Confirm:$false -server $vcenter_server > $null
WriteLog "Disconnecting from FVP Management Server: $fvp_server"
Disconnect-PrnxServer > $null
if ($error_msg -eq "") {
    WriteLog "Script completed without error"
    exit 0
} else {
    WriteLog "Script completed with error(s):"
    WriteLog $error_msg
    exit 1
}
