# Veeam Backup Script (non-VADP proxy)
# Author: James Smith
# Created: 11/02/2015
# Last Updated: v9.3 - 11/18/2016 - Andy Daniel
# v9.3 Update - Set VM policy back to WB doesn't require WaitTimeSecs.
# v9.2 Update - Major rewrite to handle VMs with datastore policy and WB external peers. Standardized
#               backup framework incorporated.
#
# v9.1 Update - Documentation changes and script cleanup. No new functionality.
#
# Notes: This script will transition VMs being backed up by Veeam into WT mode before
#        backup and transition them back to WB mode when done.
#                             
# Usage: Go to the section on pre and post scripts and enter the following strings in the boxes.
#        
#        Pre Job:
#        C:\windows\system32\WindowsPowerShell\v1.0\powershell.exe -Command C:\veeamprnx\backup-veeam.ps1 -Mode PRE_BACKUP
#        
#        Post Job:
#        C:\windows\system32\WindowsPowerShell\v1.0\powershell.exe -Command C:\veeamprnx\backup-veeam.ps1 -Mode POST_BACKUP
#       
#		 Use the Powershell line below to create the $passwordfile credential to be used later. NOTE
#		 THAT THE KEY TO THIS FILE IS CONTAINED IN THE SCRIPT AND CAN BE EASILY DECRYPTED BY USERS
#		 WITH ACCESS TO BOTH THE SCRIPT AND THE PASSWORD FILE. IT IS RECOMMENDED TO RUN THE VSC SERVICE
#		 AS A NAMED USER AND USE NTFS ACCESS CONTROLS TO LIMIT ACCESS TO THE FILES:
#		 Read-Host -AsSecureString -prompt "Enter password" | ConvertFrom-SecureString -key $([Byte[]](1..16))| Out-File fvp_enc_pass.txt 
#
Param ( 
     [string]$Mode
)

#Set these required parameters
$transition_timeout = 300 #This is the time that the script will wait for a VM to transitiont from Write Back to Write Through before erroring out.
$logfilepath = "C:\veeamprnx\" #Path to log files.
$passwordfile = "C:\veeamprnx\fvp_enc_pass.txt" #Path to the encrypted password file (see Usage section above to create).
$fvp_server = "pernixms.domain.local" #PernixData Management Server name or IP address.
$vcenter_server = "vcenter.domain.local" #vCenter Server name or IP address.
$username = "domain.local\svc_fvp" #Username with Administrative rights to vCenter/FVP.

Function WriteLog
{
   Param ([string]$logstring)
   $logstring = "$(Get-Date -format s) - " + $logstring
   $logstring | out-file -Filepath $logfile -append
}

Function transition_wb_vm ([string]$vm, [string]$wb_type, [int]$destage_timeout) {
	if(!$destage_timeout){
		$destage_timeout = 600
	}
	$prnx_vm = Get-PrnxObject "$vm" -ea Stop
	$cluster_uuid = $prnx_vm.wbState.cache[0]
	if ($prnx_vm.stats.destager.timeToDestageUS -gt 0){
		$destage_est = [int][Math]::Ceiling($prnx_vm.stats.destager.timeToDestageUS/1000000)	
	} else {
		$destage_est = 1
	}
	WriteLog "Estimated destage time for $vm is $destage_est seconds."
	if($destage_est -le $destage_timeout){
		$total_sleep = 0
		if ($wb_type -eq "datastore WB"){
			WriteLog "Adding VM specific Write Through policy for $vm to override datastore policy."
			Add-PrnxVirtualMachineToFVPCluster -FVPCluster $cluster_uuid -Name "$vm" -WriteThrough -ea Stop
		} else {
			Set-PrnxAccelerationPolicy -Name "$vm" -WriteThrough -ea Stop
		}
		while (($prnx_vm.wbInfo.wbState -gt 0) -and ($total_sleep -lt $destage_timeout)){
			$prnx_vm = Get-PrnxObject "$vm" -ea Stop
			$destage_est = [int][Math]::Ceiling($prnx_vm.stats.destager.timeToDestageUS/1000000)
			$total_sleep = $total_sleep + $destage_est
			WriteLog "Waiting $destage_est seconds for $vm to destage."
			start-sleep -s $destage_est
		}
		if ($prnx_vm.wbInfo.wbState -gt 0){
			Throw "Total estimated destage time for $vm is $destage_est seconds. This is greater than configured transition timeout of $destage_timeout seconds."
		} else {
			WriteLog "$vm successfully transitioned to Write Through."
		}
	} else {
		Throw "Estimated destage time for $vm is $destage_est seconds. This is greater than configured transition timeout of $destage_timeout seconds."
	}
}

#Initialize Variables
if ($Mode -eq "WriteThrough"){
		$backup_phase = "PRE_BACKUP"
	}elseif($Mode -eq "WriteBack"){
		$backup_phase = "POST_BACKUP"
	}else{
		$backup_phase = $Mode
	}

$logfile = $logfilepath + "\veeam_fvp_$(get-date -f yyyy_MM_dd_HH_mm_ss_fff)_$backup_phase.log"
[int]$vm_count = 0
$error_msg = ""
$vm_array = @()

Try { 
		WriteLog "Beginning script in $backup_phase phase"
		WriteLog "Retrieving encrypted password from file $passwordfile"
	}
Catch { 
		WriteLog "Error writing to log file."
		Exit 1
	}

Try { 
		[Byte[]] $key = (1..16)
		$fvp_enc_pass = Get-Content $passwordfile | ConvertTo-SecureString -Key $key
	}
Catch { 
		WriteLog "Error retrieving encrypted password"
		Exit 1
	}

Try { 
		$credential = New-Object System.Management.Automation.PsCredential($username, $fvp_enc_pass)
	}
Catch {
		WriteLog "Error creating credential object"
		Exit 1
	}

Try {
		#This bit pulls the process id of the Veeam session. From that it gets the job command used to start the job and then uses the GUID of the job to get the name of the job.
		Add-PSSnapin VMware.VimAutomation.Core -ErrorAction Stop
		Add-PSSnapin VeeamPSSnapIn -ErrorAction Stop
		WriteLog "Searching for Veeam process ID"
		$parentpid = (Get-WmiObject Win32_Process -Filter "processid='$pid'").parentprocessid.ToString()
		$parentcmd = (Get-WmiObject Win32_Process -Filter "processid='$parentpid'").CommandLine
		$veeamjob = Get-VBRJob | ?{$parentcmd -like "*"+$_.Id.ToString()+"*"}
		$jobname = $veeamjob.name
		WriteLog "Using Veeam job $jobname"
		$job = Get-VBRJob -Name "$JobName"
		$newlogfile = $logfilepath + "$jobname_" + $logfile.replace("$logfilepath","") 
		Rename-Item $logfile $newlogfile
		$logfile = $newlogfile
}
Catch {
		WriteLog "Error connecting to Veeam Server and detecting backup process: $($_.Exception.Message)"
		Exit 1
}

Try {
		WriteLog "Connecting to vCenter Server: $vcenter_server"
		$vmware = Connect-VIServer -Server $vcenter_server -credential $credential
		WriteLog "Successfully connected to vCenter Server: $($vmware.Name)"
		$objects = $job.GetObjectsInJob() 

		# This bit looks at the Veeam job and works out if it contains a list of VMs or is a selection based on a container. 
		# It checks for any exclusions if it is a container and then builds a list of VMs that are going to be backed up.
		writelog "Building list of VMs in $jobname job"
			foreach ($o in $objects) {
					if($o.object.vitype -eq "Tag") {
					  		$iq = (Get-VM -Tag $o.name) 
						  	foreach ($vminlist in $iq){
						  		if ((Get-VBRJobObject -Job $jobname -Name $vminlist.name).Object -notmatch "Name:*"){
						  			$vm_array += ,$($vminlist.Name, $vminlist.PersistentId, $vminlist.PowerState)
									writelog "Adding $($vminlist.name) [$($vminlist.PersistentId)] via Tag"
						  		}
								
							}
				  	} elseif($o.object.vitype -eq "Datastore") {
				  			$iq = (Get-VM -Datastore $o.name) 
				  			foreach ($vminlist in $iq){
				  				if ((Get-VBRJobObject -Job $jobname -Name $vminlist.name).Object -notmatch "Name:*"){
						  			$vm_array += ,$($vminlist.Name, $vminlist.PersistentId, $vminlist.PowerState)
									writelog "Adding $($vminlist.name) via [$($vminlist.PersistentId)] Datastore"
						  		}
							}
				  	} elseif($o.object.vitype -ne "VirtualMachine") {
				  			$iq = (Get-VM -Location $o.name) 
				  			foreach ($vminlist in $iq){
				  				if ((Get-VBRJobObject -Job $jobname -Name $vminlist.name).Object -notmatch "Name:*"){
						  			$vm_array += ,$($vminlist.Name, $vminlist.PersistentId, $vminlist.PowerState)
									writelog "Adding $($vminlist.name) [$($vminlist.PersistentId)] via Location"
						  		}
							}
				  	} elseif($o.type -ne "Exclude"){
				  			$vv = (Get-VM -Name $o.name)
				  			$vm_array += ,$($vv.Name, $vv.PersistentId, $vv.PowerState)
				  			writelog "Adding $($vv.Name) [$($vv.PersistentId)] via Name"
					}
		}
		
		$unique_vm_array = @()
		foreach ($vm in $vm_array){
			$dup = $false
			foreach ($unique_vm in $unique_vm_array){
				if (($vm[0] -eq $unique_vm[0]) -and ($vm[1] -eq $unique_vm[1])){$dup = $true; break}
			}
			if(!$dup){$unique_vm_array += ,@($vm)}
		}
		$pre_unique_vm_count = $vm_array.Count
		$vm_array = $unique_vm_array
		$vm_count = $vm_array.Count

}
Catch {
		WriteLog "Error retrieving VMs in backup job: $($_.Exception.Message)"
		Exit 1
}

If ($vm_count -eq 0) {
		WriteLog "No VM(s) imported from Veeam backup job"
		Exit 1
}

WriteLog "Imported $pre_unique_vm_count VM(s) from Veeam; $vm_count unique."
WriteLog "Connecting to FVP Management Server: $fvp_server"
Try {
		import-module prnxcli -ea Stop
		Connect-PrnxServer $fvp_server -Credentials $credential -ea Stop
		$prnxms = Get-PrnxObject -Type PrnxMServer
        if($prnxms.licensedFeatures -match "FVP_FEATURE_FAULT_DOMAIN"){
			$fault_domains_licensed = $true
			WriteLog "Connected to $fvp_server, version $($prnxms.buildinfo), Enterprise License"
		} else {
			WriteLog "Connected to $fvp_server, version $($prnxms.buildinfo), Standard License"
		}
	}
Catch {
		WriteLog "Error connecting to FVP Management Server: $($_.Exception.Message)"
		Exit 1
    }

$vm_index=1
if ($backup_phase -eq "PRE_BACKUP") {
	foreach ($vm in $vm_array) {
		Try {
				$prnx_vm = Get-PrnxObject $vm[0] -ea Stop
		}
		Catch {
			WriteLog "Unable to find $($vm[0]) [$($vm[1])] in FVP Inventory: $($_.Exception.Message)"
			$error_msg = "Unable to find $($vm[0]) [$($vm[1])] in FVP Inventory: $($_.Exception.Message)"
			Exit 1
		}
		if ($prnx_vm.wbInfo.wbState -eq 1) {
			if ($vm[2] -eq "PoweredOn") {
				$cluster_uuid = $prnx_vm.wbState.cache[0]
				$wb_peers = $prnx_vm.policy.numWBPeers
				if($fault_domains_licensed){
					$wb_ext_peers = $prnx_vm.policy.numWBExternalPeers
				}else{
					$wb_ext_peers = 0
				}
				$wb_type = $prnx_vm.policyStatus 
				WriteLog "Transitioning $($vm[0]) [$($vm[1])]: Currently Powered On in Writeback with $wb_peers peers, $wb_ext_peers external"
				Try {
					[Environment]::SetEnvironmentVariable("VIRTUAL_MACHINE.$vm[1].peers", $wb_peers, "user")
					[Environment]::SetEnvironmentVariable("VIRTUAL_MACHINE.$vm[1].ext_peers", $wb_ext_peers, "user")
					[Environment]::SetEnvironmentVariable("VIRTUAL_MACHINE.$vm[1].wb_type", $wb_type, "user")
				}
				Catch {
					WriteLog "Failed to create environment variable to store write back peer count: $($_.Exception.Message)"
					$error_msg = "Failed to create environment variable to store write back peer count: $($_.Exception.Message)"
					Exit 1
				}
				Try { 
					transition_wb_vm $vm[0] $wb_type $transition_timeout
				}
				Catch {
					WriteLog "Failed to transition $($vm[0]) [$($vm[1])]: $($_.Exception.Message)"
					$error_msg = "Transition Error: Failed to transition $($vm[0]) [$($vm[1])]: $($_.Exception.Message)"
					Exit 1
				}
			} else {
				WriteLog "Skipping $($vm[0]) [$($vm[1])]: Not Powered On"
			}
		} else {
			Try {
				[Environment]::SetEnvironmentVariable("VIRTUAL_MACHINE.$vm[1].peers", "WT", "user")
				[Environment]::SetEnvironmentVariable("VIRTUAL_MACHINE.$vm[1].ext_peers", "WT", "user")
				}
				Catch {
					WriteLog "Failed to create environment variable to store write back peer count: $($_.Exception.Message)"
					$error_msg = "Failed to create environment variable to store write back peer count: $($_.Exception.Message)"
					Exit 1
				}
			WriteLog "Skipping $($vm[0]) [$($vm[1])]: Not in Writeback"
		}
	$vm_index++
	}
} elseif (($backup_phase -eq "POST_BACKUP") -or ($backup_phase -eq "FAILED_BACKUP")) {
	foreach ($vm in $vm_array) {
		$wb_peers = $null
		$wb_ext_peers = $null
		$wb_type = $null
		Try {
			$prnx_vm = Get-PrnxObject $vm[0] -ea Stop
		}
		Catch {
			WriteLog "Unable to find $($vm[0]) [$($vm[1])] in FVP Inventory: $($_.Exception.Message)"
			$error_msg = "Unable to find $($vm[0]) [$($vm[1])] in FVP Inventory: $($_.Exception.Message)"
			continue
		}
		
			if ($vm[2] -eq "PoweredOn") {
			$wb_peers = [Environment]::GetEnvironmentVariable("VIRTUAL_MACHINE.$vm[1].peers", "user")
			$wb_ext_peers = [Environment]::GetEnvironmentVariable("VIRTUAL_MACHINE.$vm[1].ext_peers", "user")
			$wb_type = [Environment]::GetEnvironmentVariable("VIRTUAL_MACHINE.$vm[1].wb_type", "user")
			[Environment]::SetEnvironmentVariable("VIRTUAL_MACHINE.$vm[1].peers",$null,"user")
			[Environment]::SetEnvironmentVariable("VIRTUAL_MACHINE.$vm[1].ext_peers",$null,"user")
			[Environment]::SetEnvironmentVariable("VIRTUAL_MACHINE.$vm[1].wb_type",$null,"user")
			if (($wb_peers -eq $null) -or ($wb_ext_peers -eq $null)) {
				WriteLog "Skipping $($vm[0]) [$($vm[1])]: Unable to read previous Writeback peer count"
				$error_msg = "Skipping $($vm[0]) [$($vm[1])]: Unable to read previous Writeback peer count"
			} elseif ($wb_peers -eq "WT") {
				WriteLog "Skipping $($vm[0]) [$($vm[1])]: Not previously in Writeback"
			} elseif (($prnx_vm.wbInfo.wbState -eq 1) -and ($prnx_vm.policy.numWBPeers -eq $wb_peers) -and ($prnx_vm.policy.numWBExternalPeers -eq $wb_ext_peers)) {
				WriteLog "Skipping $($vm[0]) [$($vm[1])]: Already in Writeback with previous peer count"
			} elseif (($prnx_vm.wbInfo.wbState -ne 1) -or ($prnx_vm.policy.numWBPeers -ne $wb_peers) -or ($prnx_vm.policy.numWBExternalPeers -ne $wb_ext_peers)){
				WriteLog "Transitioning $($vm[0]) [$($vm[1])]: Previously in Writeback with $wb_peers peers, $wb_ext_peers external peers."
				Try {
					if ($wb_type -eq "datastore WB"){
						WriteLog "Removing VM specific policy for $($vm[0]) [$($vm[1])]. VM will re-inherit datastore policy."
						Remove-PrnxObjectFromFVPCluster -Object $prnx_vm -FVPCluster $prnx_vm.wbState.cache[0] -ea Stop
					} else {
						WriteLog "Setting VM specific policy for $($vm[0]) [$($vm[1])] VM to Write Back."
						if($fault_domains_licensed){
							Set-PrnxAccelerationPolicy -Object $prnx_vm -WriteBack -NumWBPeers $wb_peers -NumWBExternalPeers $wb_ext_peers -ea Stop
						} else {
							Set-PrnxAccelerationPolicy -Object $prnx_vm -WriteBack -NumWBPeers $wb_peers -ea Stop
						}
					}
				}
				Catch {
					WriteLog "Failed to transition $($vm[0]) [$($vm[1])]: $($_.Exception.Message)"
					$error_msg = "Failed to transition $($vm[0]) [$($vm[1])]: $($_.Exception.Message)"
					continue
				}
			}
		} else {
				WriteLog "Skipping $($vm[0]) [$($vm[1])]: Not Powered On"
			}
	$vm_index++
	}
} 
if ($vmware) { 
	WriteLog "Disconnecting from vCenter Server: $vcenter_server"
	Disconnect-VIServer -Confirm:$false -server $vcenter_server > $null 
}
WriteLog "Disconnecting from FVP Management Server: $fvp_server"
Disconnect-PrnxServer > $null
if ($error_msg -eq "") {
    WriteLog "Script completed without error"
    exit 0
} else {
    WriteLog "Script completed with error(s):"
    WriteLog $error_msg
    exit 1
}