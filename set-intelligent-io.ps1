# Intelligent I/O Profiling Script
# Author: Andy Daniel
# Created: 06/29/2015
#
# v0.1 - Initial version
#
# Notes: This script can be used to call intelligent I/O profiling cmdlets.
#                             
# Usage: Call this script with five parameters:
#        -Mode: "Suspend" or "Enable" to suspend or enable read or write data population
#		 -VM: Comma separated list of VM names in quotes to enable/disable
#		 -FVPServer: FVP Management Server name
#		 -User: Username of account with administrator privileges
#		 -Passfile: Full path to file containing the encrypted password (see below)
#       
#		 Use the Powershell line below to create the $passfile credential to be used later. This
#		 file must be created while logged in as the service account user that will run the script:
#		 Read-Host -AsSecureString -prompt "Enter password" | ConvertFrom-SecureString | Out-File password.txt 
#
Param(
  [Parameter(Mandatory=$True)]
  [string]$Mode,
  [Parameter(Mandatory=$True)]
  [string]$VM,
  [Parameter(Mandatory=$True)]
  [string]$FVPServer,
  [Parameter(Mandatory=$True)]
  [string]$User,
  [Parameter(Mandatory=$True)]
  [string]$PassFile
)

$fvp_enc_pass = Get-Content $PassFile | ConvertTo-SecureString
$credential = New-Object System.Management.Automation.PsCredential($User, $fvp_enc_pass)
Import-Module prnxcli
Connect-PrnxServer $FVPServer -Credentials $credential

$vms = $VM.Replace(" ","").split(",")
foreach ($vm in $vms) {
	if ($Mode -eq "Suspend"){	
		Suspend-PrnxReadWriteDataPopulation $vm
		}
	elseif ($Mode -eq "Enable"){
		Enable-PrnxReadWriteDataPopulation $vm
	}
}
#End Script
