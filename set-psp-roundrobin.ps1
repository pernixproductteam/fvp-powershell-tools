#Set Round Robin IO path switch for vendor LUNs
#Author: Andy Daniel
#Created: 02/23/2014
#Last Updated: 
#Notes: This script will modify the PSP for any LUN with a PernixData PSP and make
#		it RR. It can also modify the PSP "IOPS" parameter to switch IOs every n IOs.
#		
#Usage: 
#       

$IOSwitch = Read-Host "Enter IO count on which to switch paths [1000]"
if (!$IOSwitch) { 
  $IOSwitch = "1000"
}

Get-VMHost | where {$_.ConnectionState -eq "Maintenance"} | ForEach-Object {
$esxcli = Get-EsxCli -VMHost $_
$esxcli.storage.nmp.device.list() | Where {($_.StorageArrayType -ne "VMW_SATP_LOCAL") -and ($_.PathSelectionPolicy -like "PRNX*") -and ($_.PathSelectionPolicy -ne "PRNX_PSP_RR")} | group-object Device | %{
        $esxcli.storage.nmp.device.set($null, $_.Name, "PRNX_PSP_RR")
}
$esxcli.storage.nmp.device.list() | Where {($_.StorageArrayType -ne "VMW_SATP_LOCAL") -and ($_.PathSelectionPolicy -eq "PRNX_PSP_RR")} | group-object Device | %{
        $esxcli.storage.nmp.psp.generic.deviceconfig.set($false, "type=iops,iops=$IOSwitch", $_.Name)
}
}