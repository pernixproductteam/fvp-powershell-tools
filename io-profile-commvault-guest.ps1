# Commvault Guest Intelligent I/O Profiling
# Author: Andy Daniel
# Created: 11/30/2015
# Last Updated: v0.1 - 12/02/2015 - Andy Daniel
# v0.1 Update - Added ability to pass VM's name as specified in vCenter for situations where it doesn't match Commvault's "cn" parameter name.
#
# Notes: This script will enable Intelligent I/O Profiling to suspend data population for VMs being backed up 
#        Commvault and transition them back when done.
#                             
# Usage: Install or copy the PernixData Powershell module onto the guest VM and create an encrypted password file (see insructions below).
#		 You may also need to set the Powershell execution policy to "unrestricted" on the VM.
#		 Set the required parameters in the script below and then copy to the VM.
#		 Set Commvault "Run As" in the "Pre/Post Process" section of the Commvault subclient to Local System Account.
#        Call this script in the "Pre/Post Process" section of the Commvault subclient. Call powershell.exe using EXACT SYNTAX below and
#		 provide the -Phase parameter as appropriate:
#
#        C:\Windows\System32\WindowsPowerShell\v1.0\powershell.exe "& 'C:\io-profile-commvault-guest.ps1' "-Phase pre"
#        OR
#		 C:\Windows\System32\WindowsPowerShell\v1.0\powershell.exe "& 'C:\io-profile-commvault-guest.ps1' "-vCenterVMName sql01.pernixdata.com -Phase pre"
#
#		 Use the Powershell line below to create the $passwordfile credential to be used later. NOTE
#		 THAT THE KEY TO THIS FILE IS CONTAINED IN THE SCRIPT AND CAN BE EASILY DECRYPTED BY USERS
#		 WITH ACCESS TO BOTH THE SCRIPT AND THE PASSWORD FILE. IT IS RECOMMENDED TO 
#		 USE NTFS ACCESS CONTROLS TO LIMIT ACCESS TO THE FILES:
#		 Read-Host -AsSecureString -prompt "Enter password" | ConvertFrom-SecureString -key $([Byte[]](1..16))| Out-File fvp_enc_pass.txt 
#
Param(
  [string]$Phase, #This is the phase of the backup, either "pre" or "post".
  [string]$vCenterVMName #This is the name of the VM as it appears in vCenter. If provided, this overrides the hostname provided by Commvault.
)

#Set these required parameters
$transition_timeout = 300 #This is the time that the script will wait for a VM to transition before erroring out.
$logfilepath = "C:\log" #Path to log files.
$passwordfile = "C:\fvp_enc_pass.txt" #Path to the encrypted password file (see Usage section above to create).
$fvp_server = "pernixms01.pernixdata.com" #PernixData Management Server name or IP address.
$fvp_username = "domain\svc_account" #Username with Administrative rights to vCenter/FVP.

#Initialize Variables
$logfile = $logfilepath + "\io_profile_commvault_guest_$(get-date -f yyyy_MM_dd_HH_mm_ss)_$phase.log"

Function WriteLog
{
   Param ([string]$logstring)
   $logstring = "$(Get-Date -format s) - " + $logstring
   $logstring | out-file -Filepath $logfile -append
}

Function transition_vm ([string]$vm, [int]$timeout) {
	if(!$timeout){
		$timeout = 600
	}
	$total_sleep = 0
	$prnx_vm = Get-PrnxObject "$vm" -ea Stop
	Suspend-PrnxReadWriteDataPopulation -Name $vm -ea Stop
	while (($prnx_vm.cachePolicy -ne 531) -and ($total_sleep -lt $timeout)){
		$prnx_vm = Get-PrnxObject "$vm" -ea Stop
		$total_sleep = $total_sleep + 5
		WriteLog "Waiting 5 seconds for $vm to transition."
		start-sleep -s 5
	}
	if ($prnx_vm.cachePolicy -ne 531){
		Throw "$vm did not transition within timeout of $timeout seconds."
	} else {
		WriteLog "Intelligent I/O Filtering successfully enabled for $vm."
	}
}

Try { 
		WriteLog "Beginning script in $phase phase"
		WriteLog "Retrieving encrypted password from file $passwordfile"
	}
Catch { 
		Exit 1
	}

Try { 
		[Byte[]] $key = (1..16)
        $fvp_enc_pass = Get-Content $passwordfile | ConvertTo-SecureString -Key $key
	}
Catch { 
		WriteLog "Error retrieving encrypted password"
		Exit 1
	}

Try { 
		$credential = New-Object System.Management.Automation.PsCredential($fvp_username, $fvp_enc_pass)
	}
Catch {
		WriteLog "Error creating credential object"
		Exit 1
	}
WriteLog "Connecting to FVP Management Server: $fvp_server"
Try {
		import-module prnxcli -ea Stop
		Connect-PrnxServer $fvp_server -Credentials $credential -ea Stop
		$prnxms = Get-PrnxObject -Type PrnxMServer
        if($prnxms.licensedFeatures -match "FVP_FEATURE_FAULT_DOMAIN"){
			$fault_domains_licensed = $true
			WriteLog "Connected to $fvp_server, version $($prnxms.buildinfo), Enterprise License"
		} else {
			WriteLog "Connected to $fvp_server, version $($prnxms.buildinfo), Standard License"
		}
	}
Catch {
		WriteLog "Error connecting to FVP Management Server: $($_.Exception.Message)"
		Exit 1
    }

if(!$vCenterVMName){
	$vCenterVMName = $args[$([array]::IndexOf($args, '-cn') + 1)]
	WriteLog "No vCenterVMName specified, using Commvault provided name of $vCenterVMName."
}

if ($phase -eq "pre") {
		Try {
				$prnx_vm = Get-PrnxObject $vCenterVMName -ea Stop
		}
		Catch {
			WriteLog "Unable to find $vCenterVMName in FVP Inventory: $($_.Exception.Message)"
			WriteLog "Disconnecting from FVP Management Server: $fvp_server"
			Disconnect-PrnxServer > $null
			Exit 1
		}
		if ($prnx_vm.cachePolicy -ne 531) {
				WriteLog "Enabling Intelligent I/O Filtering for $vCenterVMName."
				Try { 
					transition_vm $vCenterVMName $transition_timeout
				}
				Catch {
					WriteLog "Failed to enable Intelligent I/O Filtering for $vCenterVMName : $($_.Exception.Message)"
					WriteLog "Disconnecting from FVP Management Server: $fvp_server"
					Disconnect-PrnxServer > $null
					Exit 1
				}
		} else {
			WriteLog "Skipping $vCenterVMName : Intelligent I/O Filtering already enabled."
		}
} elseif ($phase -eq "post"){
	Try {
		WriteLog "Disabling Intelligent I/O Filtering for $vCenterVMName to resume data population."
		Resume-PrnxReadWriteDataPopulation -Name $vCenterVMName -ea Stop
		}
	Catch {
			WriteLog "Error disabling Intelligent I/O Filtering to resume data population."
			WriteLog "Disconnecting from FVP Management Server: $fvp_server"
			Disconnect-PrnxServer > $null
			Exit 1
		}
} else {
		WriteLog "No valid backup phase provided. Backup phase of 'pre' or 'post' should be provided via -Phase parameter."
		WriteLog "Disconnecting from FVP Management Server: $fvp_server"
		Disconnect-PrnxServer > $null
		Exit 1
}

WriteLog "Disconnecting from FVP Management Server: $fvp_server"
Disconnect-PrnxServer > $null
WriteLog "Script completed without error"
Exit 0
