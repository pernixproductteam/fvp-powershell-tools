################################################################################################################
#
# PernixData FVP - Virtual Machine Stats Export
# Author: Andy Daniel
# Created: 07/03/2015
#          
# Notes: This script will export FVP stats for given VMs. Script detects presence of connected prnx-server
#		 and will prompt for one if not already connected.
#        
# Usage: Run this script on a host that has Powershell v3 and the FVP Powershell module installed.
#		Call this script with five parameters:
#       VM: One or more VM names separated by a comma. (Required)
#		Days: How many days back you'd like to collect. Default: 30
#		Rollup:   When specified stats are aggregated per VM. Default: False
#				  higher in environments with slow storage will reduce false positives. Acceptable
#				  values are 0 to 25 percent. Default when not specified is 5 percent.
#       CSV: Path to a CSV file to export as CSV, if not specified, output to a grid-view.
#
# Example: To collect stats for a single VM:
#			.\get-fvp-stats-range.ps1 -vm exampleVM -days 7
#		   To collect stats for all FVP VMS:
#			Connect-PrnxServer localhost; .\get-fvp-stats-range.ps1 -vm (get-prnxvm).Name -days 1 -rollup
#       
################################################################################################################

Param(
  [Parameter(Mandatory=$true,Position=0)]
  [string[]]$VM,
  [int]$Days,
  [switch]$Rollup,
  [string]$CSV,
  [Date]$Start,
  [Date]$End
)

function Get-EpochTime{
	Param([Datetime]$time)
	$s = (Get-Date $time).ToUniversalTime()
	$s2 = New-Object DateTime -ArgumentList 1970, 1, 1
	return ($s - $s2).TotalMilliseconds
}

function Get-VMPrnxStats{
	Param([string]$uuid, [bool]$aggregate)
	$i=1
	$samples = @()
	$request = New-Object prnx_QueryStatsRequest
	$request.AggregateStats = $aggregate
	$request.BreakDownId = "READ,WRITE,SYSTEM,LOCAL_FLASH,REMOTE_FLASH,SAN,FLASH_READ,FLASH_WRITE,SAN_READ,SAN_WRITE,NET_READ,NET_WRITE"
	$counters = New-Object "System.Collections.Generic.List[String]"
	$counters.Add("IOPS")
	$counters.Add("LATENCY")
	$counters.Add("THROUGHPUT")
	$counters.Add("USAGE")
	$counters.Add("HIT_RATE")
	$counters.Add("EVICTION_RATE")
	$counters.Add("TIME_TO_DESTAGE")
	$counters.Add("DESTAGER_SIZE")
	$request.CounterId = $counters
	$request.HostFilterUuid = $null
	$request.NumberOfSamples = 100
	$request.SortOrder = "ASC"
	$request.StartTime =  Get-EpochTime -time $sT
	$request.EndTime = Get-EpochTime -time $eT
	$ids = New-Object "System.Collections.Generic.List[prnx_ObjId]"
	$id = New-Object prnx_ObjId
	$id.Uuid = [PrnxCli.ConvertUtils]::ToPrnxUuid($uuid)
	$ids.Add($id)
	$request.ObjectId = $ids
	$s = $DefaultPrnxServer.Client.queryStats($request)
	$s.Keys | % { 
		$objStats = $s.Item($_)
		$statsRecords = $objStats.Stats
		$totalrecs = $statsRecords.Count
		$statsRecords | % {
			$eachRecord = $_
			Write-Progress -id 2 -activity "Virtual Machine:" -status "$($v)" -PercentComplete $(($i/$totalrecs)*100)
			$sampleTime = [PrnxCli.ConvertUtils]::ConvertJavaMiliSecondToDateTime($eachRecord.Timestamp)
			$counterData = $eachRecord.CounterData
			$row = "" | Select VM,Time,USAGE,HIT_RATE,EVICTION_RATE,LATENCY_TOTAL,LATENCY_READ,LATENCY_WRITE,LATENCY_SYSTEM,LATENCY_LOCAL_FLASH,LATENCY_REMOTE_FLASH,LATENCY_SAN,LATENCY_FLASH_READ,LATENCY_FLASH_WRITE,LATENCY_SAN_READ,LATENCY_SAN_WRITE,LATENCY_NET_READ,LATENCY_NET_WRITE,IOPS_TOTAL,IOPS_READ,IOPS_WRITE,IOPS_SYSTEM,IOPS_LOCAL_FLASH,IOPS_REMOTE_FLASH,IOPS_SAN,IOPS_FLASH_READ,IOPS_FLASH_WRITE,IOPS_SAN_READ,IOPS_SAN_WRITE,IOPS_NET_READ,IOPS_NET_WRITE,THROUGHPUT_TOTAL,THROUGHPUT_READ,THROUGHPUT_WRITE,THROUGHPUT_SYSTEM,THROUGHPUT_LOCAL_FLASH,THROUGHPUT_REMOTE_FLASH,THROUGHPUT_SAN,THROUGHPUT_FLASH_READ,THROUGHPUT_FLASH_WRITE,THROUGHPUT_SAN_READ,THROUGHPUT_SAN_WRITE,THROUGHPUT_NET_READ,THROUGHPUT_NET_WRITE
			$row.VM = $v
			$row.Time = $sampleTime
			$row.USAGE = [math]::Round(($counterData["USAGE"].Data / 1GB),1)
			$row.HIT_RATE = $counterData["HIT_RATE"].Data
			$row.EVICTION_RATE = $counterData["EVICTION_RATE"].Data
			$row.IOPS_TOTAL = $counterData["IOPS"].Data
			$row.IOPS_READ = $counterData.IOPS.BreakDownData.READ
			$row.IOPS_WRITE = $counterData.IOPS.BreakDownData.WRITE
			$row.IOPS_SYSTEM = $counterData.IOPS.BreakDownData.SYSTEM
			$row.IOPS_LOCAL_FLASH = $counterData.IOPS.BreakDownData.LOCAL_FLASH
			$row.IOPS_REMOTE_FLASH = $counterData.IOPS.BreakDownData.REMOTE_FLASH
			$row.IOPS_SAN = $counterData.IOPS.BreakDownData.SAN
			$row.IOPS_FLASH_READ = $counterData.IOPS.BreakDownData.FLASH_READ 
			$row.IOPS_FLASH_WRITE = $counterData.IOPS.BreakDownData.FLASH_WRITE
			$row.IOPS_SAN_READ = $counterData.IOPS.BreakDownData.SAN_READ
			$row.IOPS_SAN_WRITE = $counterData.IOPS.BreakDownData.SAN_WRITE
			$row.IOPS_NET_READ = $counterData.IOPS.BreakDownData.NET_READ
			$row.IOPS_NET_WRITE = $counterData.IOPS.BreakDownData.NET_WRITE
			$row.THROUGHPUT_TOTAL = $counterData["THROUGHPUT"].Data
			$row.THROUGHPUT_READ = $counterData.THROUGHPUT.BreakDownData.READ
			$row.THROUGHPUT_WRITE = $counterData.THROUGHPUT.BreakDownData.WRITE
			$row.THROUGHPUT_SYSTEM = $counterData.THROUGHPUT.BreakDownData.SYSTEM
			$row.THROUGHPUT_LOCAL_FLASH = $counterData.THROUGHPUT.BreakDownData.LOCAL_FLASH
			$row.THROUGHPUT_REMOTE_FLASH = $counterData.THROUGHPUT.BreakDownData.REMOTE_FLASH
			$row.THROUGHPUT_SAN = $counterData.THROUGHPUT.BreakDownData.SAN
			$row.THROUGHPUT_FLASH_READ = $counterData.THROUGHPUT.BreakDownData.FLASH_READ 
			$row.THROUGHPUT_FLASH_WRITE = $counterData.THROUGHPUT.BreakDownData.FLASH_WRITE
			$row.THROUGHPUT_SAN_READ = $counterData.THROUGHPUT.BreakDownData.SAN_READ
			$row.THROUGHPUT_SAN_WRITE = $counterData.THROUGHPUT.BreakDownData.SAN_WRITE
			$row.THROUGHPUT_NET_READ = $counterData.THROUGHPUT.BreakDownData.NET_READ
			$row.THROUGHPUT_NET_WRITE = $counterData.THROUGHPUT.BreakDownData.NET_WRITE
			$row.LATENCY_TOTAL = [math]::Round(($counterData["LATENCY"].Data/1000),1)
			$row.LATENCY_READ = [math]::Round(($counterData.LATENCY.BreakDownData.READ/1000),1)
			$row.LATENCY_WRITE = [math]::Round(($counterData.LATENCY.BreakDownData.WRITE/1000),1)
			$row.LATENCY_SYSTEM = [math]::Round(($counterData.LATENCY.BreakDownData.SYSTEM/1000),1)
			$row.LATENCY_LOCAL_FLASH = [math]::Round(($counterData.LATENCY.BreakDownData.LOCAL_FLASH/1000),1)
			$row.LATENCY_REMOTE_FLASH = [math]::Round(($counterData.LATENCY.BreakDownData.REMOTE_FLASH/1000),1)
			$row.LATENCY_SAN = [math]::Round(($counterData.LATENCY.BreakDownData.SAN/1000),1)
			$row.LATENCY_FLASH_READ = [math]::Round(($counterData.LATENCY.BreakDownData.FLASH_READ/1000),1)
			$row.LATENCY_FLASH_WRITE = [math]::Round(($counterData.LATENCY.BreakDownData.FLASH_WRITE/1000),1)
			$row.LATENCY_SAN_READ = [math]::Round(($counterData.LATENCY.BreakDownData.SAN_READ/1000),1)
			$row.LATENCY_SAN_WRITE = [math]::Round(($counterData.LATENCY.BreakDownData.SAN_WRITE/1000),1)
			$row.LATENCY_NET_READ = [math]::Round(($counterData.LATENCY.BreakDownData.NET_READ/1000),1)
			$row.LATENCY_NET_WRITE = [math]::Round(($counterData.LATENCY.BreakDownData.NET_WRITE/1000),1)
			$samples += $row
			$i++
			}
	}
	return $samples
}

if(-not $Start){
$eT = Get-Date; #Current time
} else {
$eT = $Start
}
if(-not $End){
$sT = $eT.AddDays(-$Days)
} else {
$sT = $End
}
$report = @()

Try { 
	Get-Module prnxcli -ea stop | Out-Null
	}
Catch {
	Try { 
		Write-Host "Importing PernixData Powershell Module"
		import-module prnxcli -ea stop | Out-Null	
	}
	Catch { 
		Write-Host "Error importing PernixData Powershell Module. This module can be installed by running the FVP Management Server installer and choosing 'Advanced'. - Exiting" -fo Red
		Exit
	}
}

## Connect to FVP Management Server
Clear-Host
Write-Host
Write-Host "PernixData FVP - Virtual Machine Stats Export"
while (-not $DefaultPrnxServer){
    $fvp_servername = Read-Host "Enter FVP Management Server name"
    $user_creds = Get-Credential -Message "Enter a username & password with at least FVP read-only rights"
    Write-Host "Connecting to FVP Management Server: $fvp_servername" -fo Yellow
    Connect-Prnxserver $fvp_servername -Credential $user_creds -ea silentlycontinue | Out-Null
    if (-not $?){
    	Write-Host "Unable to connect to $fvp_servername." -fo Red
    	Write-Host
		do {$continue = Read-Host "Verify FVP Management Server connectivity & permissions, then press c to continue or e to exit"} until (($continue.ToLower() -eq "c") -or ($continue.ToLower() -eq "e"))
		if ($continue.ToLower() -eq "e") {Write-Host "You chose to exit." -fo yellow; exit;}
		Write-Host
    }
    else {
		Write-Host "Connected to $fvp_servername" -fo Yellow
	}
}

If ($Days -eq ""){
	$Days = 30
}

if ($VM){
	$j=1
	$totalVMs = $VM.Count
	foreach ($v in $VM){
			Write-Progress -id 1 -activity "PernixData FVP - Virtual Machine Stats Export" -status "Collecting Stats" -PercentComplete $(($j/$totalVMs)*100)
			$prnxVM = Get-PrnxObject "$v" -ea SilentlyContinue | where {$_.Type -eq "VM"}
		if ($prnxVM){
			$uuid = $prnxVM.Uuid
			$report += Get-VMPrnxStats $uuid $Rollup
		} else {
			Write-Host "Unable to find an FVP accelerated VM named $v." -fo Red
		}
		$j++
	}
} else {
	Write-Host "VM(s) not found. Check parameter input." -fo Red
}

Disconnect-Prnxserver
if ($CSV) {
	$report | sort VM, Time -desc | export-csv $CSV
} else {
	$report | sort VM, Time -desc | out-gridview
}



