# EqualLogic Backup Script
# Author: Andy Daniel
# Created: 05/15/2017
#
# Notes: This script will transition VMs being snapshotted on an EQL array into WT mode before
#        snapshot and transition them back to WB mode when done. The volume name must be the
#		 same between VMware and EqualLogic.
#                             
# Usage: Use Windows Scheduler to run this script to create new snapshots or replicas.
#        
#        powershell -command "& 'C:\eqlrnx\backup-equallogic.ps1' -Volume EQL_Prod_1 -RecoveryPoints 3 -Replication"  
#        
#		 Use the Powershell line below to create the $passwordfile credential to be used later. NOTE
#		 THAT THE KEY TO THIS FILE IS CONTAINED IN THE SCRIPT AND CAN BE EASILY DECRYPTED BY USERS
#		 WITH ACCESS TO BOTH THE SCRIPT AND THE PASSWORD FILE. IT IS RECOMMENDED TO RUN THE SCHEDULED TASK
#		 AS A NAMED USER AND USE NTFS ACCESS CONTROLS TO LIMIT ACCESS TO THE FILES:
#		 Read-Host -AsSecureString -prompt "Enter password" | ConvertFrom-SecureString -key $([Byte[]](1..16))| Out-File fvp_enc_pass.txt 
#
Param ( 
     [string]$Volume,
     [int]$RecoveryPoints,
     [bool]$Replication
)

#Set these required parameters
$transition_timeout = 300 #This is the time that the script will wait for a VM to transitiont from Write Back to Write Through before erroring out.
$logfilepath = "C:\eqlprnx\" #Path to log files.
$passwordfile = "C:\eqlprnx\fvp_enc_pass.txt" #Path to the encrypted password file (see Usage section above to create).
$fvp_server = "pernixms.domain.local" #PernixData Management Server name or IP address.
$vcenter_server = "vcenter.domain.local" #vCenter Server name or IP address.
$username = "domain.local\svc_fvp" #Username with Administrative rights to vCenter/FVP.
$eql_module_path = "C:\eqlprnx\EqlPSTools.dll"
$eql_group_address = "192.168.1.5" #EqualLogic group IP address.
$eql_replication_site = "DR Site" #The name of a PS Series group that has been configured as the replication partner
$eql_username = "domain.local\eql_admin" #Username with Administrative rights to vCenter/FVP.
$eql_passwordfile = "C:\eqlprnx\eql_enc_pass.txt" #Path to the encrypted password file (see Usage section above to create).

Function WriteLog
{
   Param ([string]$logstring)
   $logstring = "$(Get-Date -format s) - " + $logstring
   $logstring | out-file -Filepath $logfile -append
}

Function transition_wb_vm ([string]$vm, [string]$wb_type, [int]$destage_timeout) {
	if(!$destage_timeout){
		$destage_timeout = 600
	}
	$prnx_vm = Get-PrnxObject "$vm" -ea Stop
	$cluster_uuid = $prnx_vm.wbState.cache[0]
	if ($prnx_vm.stats.destager.timeToDestageUS -gt 0){
		$destage_est = [int][Math]::Ceiling($prnx_vm.stats.destager.timeToDestageUS/1000000)	
	} else {
		$destage_est = 1
	}
	WriteLog "Estimated destage time for $vm is $destage_est seconds."
	if($destage_est -le $destage_timeout){
		$total_sleep = 0
		if ($wb_type -eq "datastore WB"){
			WriteLog "Adding VM specific Write Through policy for $vm to override datastore policy."
			Add-PrnxVirtualMachineToFVPCluster -FVPCluster $cluster_uuid -Name "$vm" -WriteThrough -ea Stop
		} else {
			Set-PrnxAccelerationPolicy -Name "$vm" -WriteThrough -ea Stop
		}
		while (($prnx_vm.wbInfo.wbState -gt 0) -and ($total_sleep -lt $destage_timeout)){
			$prnx_vm = Get-PrnxObject "$vm" -ea Stop
			$destage_est = [int][Math]::Ceiling($prnx_vm.stats.destager.timeToDestageUS/1000000)
			$total_sleep = $total_sleep + $destage_est
			WriteLog "Waiting $destage_est seconds for $vm to destage."
			start-sleep -s $destage_est
		}
		if ($prnx_vm.wbInfo.wbState -gt 0){
			Throw "Total estimated destage time for $vm is $destage_est seconds. This is greater than configured transition timeout of $destage_timeout seconds."
		} else {
			WriteLog "$vm successfully transitioned to Write Through."
		}
	} else {
		Throw "Estimated destage time for $vm is $destage_est seconds. This is greater than configured transition timeout of $destage_timeout seconds."
	}
}

#Initialize Variables
$logfile = $logfilepath + "\eql_fvp_$(get-date -f yyyy_MM_dd_HH_mm_ss_fff).log"
[int]$vm_count = 0
$error_msg = ""
$vm_array = @()

Try { 
		WriteLog "Beginning script."
		WriteLog "Retrieving encrypted password from file $passwordfile"
	}
Catch { 
		WriteLog "Error writing to log file."
		Exit 1
	}

Try { 
		[Byte[]] $key = (1..16)
		$fvp_enc_pass = Get-Content $passwordfile | ConvertTo-SecureString -Key $key
		$eql_enc_pass = Get-Content $eql_passwordfile | ConvertTo-SecureString -Key $key
	}
Catch { 
		WriteLog "Error retrieving encrypted password"
		Exit 1
	}

Try { 
		$credential = New-Object System.Management.Automation.PsCredential($username, $fvp_enc_pass)
		$eql_credential = New-Object System.Management.Automation.PsCredential($eql_username, $eql_enc_pass)
	}
Catch {
		WriteLog "Error creating credential object"
		Exit 1
	}

Try {
		Add-PSSnapin VMware.VimAutomation.Core -ErrorAction Stop
		$jobname = $Volume
		WriteLog "Using volume $jobname"
		$newlogfile = $logfilepath + "$jobname_" + $logfile.replace("$logfilepath","") 
		Rename-Item $logfile $newlogfile
		$logfile = $newlogfile
}
Catch {
		WriteLog "Error adding VMware snapin and detecting volume: $($_.Exception.Message)"
		continue
}

Try {
		WriteLog "Connecting to vCenter Server: $vcenter_server"
		$vmware = Connect-VIServer -Server $vcenter_server -credential $credential
		WriteLog "Successfully connected to vCenter Server: $($vmware.Name)"
		writelog "Building list of VMs on $Volume datastore"
		$myDatastore = Get-Datastore -Name "$Volume"
		$vm_array = Get-VM -Datastore $myDatastore
		$vm_count = $vm_array.Count
}
Catch {
		WriteLog "Error retrieving VMs on VMware datastore: $($_.Exception.Message)"
		continue
}

If ($vm_count -eq 0) {
		WriteLog "No VM(s) imported from VMware datastore"
		continue
}

WriteLog "Imported $vm_count VM(s) from vCenter."
WriteLog "Connecting to FVP Management Server: $fvp_server"
Try {
		import-module prnxcli -ea Stop
		Connect-PrnxServer $fvp_server -Credentials $credential -ea Stop
		$prnxms = Get-PrnxObject -Type PrnxMServer
        if($prnxms.licensedFeatures -match "FVP_FEATURE_FAULT_DOMAIN"){
			$fault_domains_licensed = $true
			WriteLog "Connected to $fvp_server, version $($prnxms.buildinfo), Enterprise License"
		} else {
			WriteLog "Connected to $fvp_server, version $($prnxms.buildinfo), Standard License"
		}
	}
Catch {
		WriteLog "Error connecting to FVP Management Server: $($_.Exception.Message)"
		continue
    }

WriteLog "Connecting to EqualLogic Storage Group: $eql_group_address"
Try {
		import-module -name $eql_module_path -ea Stop
		Connect-EqlGroup -GroupAddress $eql_group_address -Credential $eql_credential -ea Stop
		WriteLog "Connected to $eql_group_address"
	}
Catch {
		WriteLog "Error connecting to EqualLogic Storage Group: $($_.Exception.Message)"
		Exit 1
    }
   
$vm_index=1
	foreach ($vm in $vm_array) {
		Try {
				$prnx_vm = Get-PrnxObject $vm.Name -ea Stop
		}
		Catch {
			WriteLog "Unable to find $($vm.Name) [$($vm.Uid)] in FVP Inventory: $($_.Exception.Message)"
			$error_msg = "Unable to find $($vm.Name) [$($vm.Uid)] in FVP Inventory: $($_.Exception.Message)"
			continue
		}
		if ($prnx_vm.wbInfo.wbState -eq 1) {
			if ($vm.PowerState -eq "PoweredOn") {
				$cluster_uuid = $prnx_vm.wbState.cache[0]
				$wb_peers = $prnx_vm.policy.numWBPeers
				if($fault_domains_licensed){
					$wb_ext_peers = $prnx_vm.policy.numWBExternalPeers
				}else{
					$wb_ext_peers = 0
				}
				$wb_type = $prnx_vm.policyStatus 
				WriteLog "Transitioning $($vm.Name) [$($vm.Uid)]: Currently Powered On in Writeback with $wb_peers peers, $wb_ext_peers external"
				Try {
					[Environment]::SetEnvironmentVariable("VIRTUAL_MACHINE.$vm.Uid.peers", $wb_peers, "user")
					[Environment]::SetEnvironmentVariable("VIRTUAL_MACHINE.$vm.Uid.ext_peers", $wb_ext_peers, "user")
					[Environment]::SetEnvironmentVariable("VIRTUAL_MACHINE.$vm.Uid.wb_type", $wb_type, "user")
				}
				Catch {
					WriteLog "Failed to create environment variable to store write back peer count: $($_.Exception.Message)"
					$error_msg = "Failed to create environment variable to store write back peer count: $($_.Exception.Message)"
					continue
				}
				Try { 
					transition_wb_vm $vm.Name $wb_type $transition_timeout
				}
				Catch {
					WriteLog "Failed to transition $($vm.Name) [$($vm.Uid)]: $($_.Exception.Message)"
					$error_msg = "Transition Error: Failed to transition $($vm.Name) [$($vm.Uid)]: $($_.Exception.Message)"
					continue
				}
			} else {
				WriteLog "Skipping $($vm.Name) [$($vm.Uid)]: Not Powered On"
			}
		} else {
			Try {
				[Environment]::SetEnvironmentVariable("VIRTUAL_MACHINE.$vm.Uid.peers", "WT", "user")
				[Environment]::SetEnvironmentVariable("VIRTUAL_MACHINE.$vm.Uid.ext_peers", "WT", "user")
				}
				Catch {
					WriteLog "Failed to create environment variable to store write back peer count: $($_.Exception.Message)"
					$error_msg = "Failed to create environment variable to store write back peer count: $($_.Exception.Message)"
					continue
				}
			WriteLog "Skipping $($vm.Name) [$($vm.Uid)]: Not in Writeback"
		}
	$vm_index++
	}

if($Replicate){
	Try {
		New-EqlReplica -GroupAddress $eql_group_address -VolumeName $Volume -ReplicationSite $eql_replication_site -ea Stop
		#Remove old replicas
		$VolReplicas = Get-EqlReplica -GroupAddress $eql_group_address -VolumeName $Volume -ReplicationSite $eql_replication_site -ea Stop
		if ($VolReplicas.Count > $RecoveryPoints) {
			Remove-EqlReplica -GroupAddress $eql_group_address -VolumeName $Volume -ReplicationSite $eql_replication_site -SelectOldest $true -Force $true -ea Stop
		}

	}
	Catch {
		WriteLog "Unable to replicate $Volume or delete oldest replica: $($_.Exception.Message)"
		$error_msg = "Unable to replicate $Volume or delete oldest replica: $($_.Exception.Message)"
		continue
	}

} else {
	Try {
			New-EqlSnapshot -GroupAddress $eql_group_address -VolumeName $Volume -ea Stop
			#Remove old snapshots
			$VolSnaps = Get-EqlSnapshot -GroupAddress $eql_group_address -VolumeName $Volume -ea Stop
			if ($VolSnaps.Count > $RecoveryPoints) {
				Remove-EqlSnapshot -GroupAddress $eql_group_address -VolumeName $Volume -SelectOldest $true -ea Stop
			}

		}
		Catch {
			WriteLog "Unable to snapshot $Volume or delete oldest snapshot: $($_.Exception.Message)"
			$error_msg = "Unable to snapshot $Volume or delete oldest snapshot: $($_.Exception.Message)"
			continue
		}
}

$vm_index=1
	foreach ($vm in $vm_array) {
		$wb_peers = $null
		$wb_ext_peers = $null
		$wb_type = $null
		Try {
			$prnx_vm = Get-PrnxObject $vm.Name -ea Stop
		}
		Catch {
			WriteLog "Unable to find $($vm.Name) [$($vm.Uid)] in FVP Inventory: $($_.Exception.Message)"
			$error_msg = "Unable to find $($vm.Name) [$($vm.Uid)] in FVP Inventory: $($_.Exception.Message)"
			continue
		}
		
			if ($vm.PowerState -eq "PoweredOn") {
			$wb_peers = [Environment]::GetEnvironmentVariable("VIRTUAL_MACHINE.$vm.Uid.peers", "user")
			$wb_ext_peers = [Environment]::GetEnvironmentVariable("VIRTUAL_MACHINE.$vm.Uid.ext_peers", "user")
			$wb_type = [Environment]::GetEnvironmentVariable("VIRTUAL_MACHINE.$vm.Uid.wb_type", "user")
			[Environment]::SetEnvironmentVariable("VIRTUAL_MACHINE.$vm.Uid.peers",$null,"user")
			[Environment]::SetEnvironmentVariable("VIRTUAL_MACHINE.$vm.Uid.ext_peers",$null,"user")
			[Environment]::SetEnvironmentVariable("VIRTUAL_MACHINE.$vm.Uid.wb_type",$null,"user")
			if (($wb_peers -eq $null) -or ($wb_ext_peers -eq $null)) {
				WriteLog "Skipping $($vm.Name) [$($vm.Uid)]: Unable to read previous Writeback peer count"
				$error_msg = "Skipping $($vm.Name) [$($vm.Uid)]: Unable to read previous Writeback peer count"
			} elseif ($wb_peers -eq "WT") {
				WriteLog "Skipping $($vm.Name) [$($vm.Uid)]: Not previously in Writeback"
			} elseif (($prnx_vm.wbInfo.wbState -eq 1) -and ($prnx_vm.policy.numWBPeers -eq $wb_peers) -and ($prnx_vm.policy.numWBExternalPeers -eq $wb_ext_peers)) {
				WriteLog "Skipping $($vm.Name) [$($vm.Uid)]: Already in Writeback with previous peer count"
			} elseif (($prnx_vm.wbInfo.wbState -ne 1) -or ($prnx_vm.policy.numWBPeers -ne $wb_peers) -or ($prnx_vm.policy.numWBExternalPeers -ne $wb_ext_peers)){
				WriteLog "Transitioning $($vm.Name) [$($vm.Uid)]: Previously in Writeback with $wb_peers peers, $wb_ext_peers external peers."
				Try {
					if ($wb_type -eq "datastore WB"){
						WriteLog "Removing VM specific policy for $($vm.Name) [$($vm.Uid)]. VM will re-inherit datastore policy."
						Remove-PrnxObjectFromFVPCluster -Object $prnx_vm -FVPCluster $prnx_vm.wbState.cache[0] -ea Stop
					} else {
						WriteLog "Setting VM specific policy for $($vm.Name) [$($vm.Uid)] VM to Write Back."
						if($fault_domains_licensed){
							Set-PrnxAccelerationPolicy -Object $prnx_vm -WriteBack -NumWBPeers $wb_peers -NumWBExternalPeers $wb_ext_peers -ea Stop
						} else {
							Set-PrnxAccelerationPolicy -Object $prnx_vm -WriteBack -NumWBPeers $wb_peers -ea Stop
						}
					}
				}
				Catch {
					WriteLog "Failed to transition $($vm.Name) [$($vm.Uid)]: $($_.Exception.Message)"
					$error_msg = "Failed to transition $($vm.Name) [$($vm.Uid)]: $($_.Exception.Message)"
					continue
				}
			}
		} else {
				WriteLog "Skipping $($vm.Name) [$($vm.Uid)]: Not Powered On"
			}
	$vm_index++
	}


 
if ($vmware) { 
	WriteLog "Disconnecting from vCenter Server: $vcenter_server"
	Disconnect-VIServer -Confirm:$false -server $vcenter_server > $null 
}

WriteLog "Disconnecting from EqualLogic PS Group: $eql_group_address"
Disconnect-EqlGroup -GroupAddress $eql_group_address > $null

WriteLog "Disconnecting from FVP Management Server: $fvp_server"
Disconnect-PrnxServer > $null

if ($error_msg -eq "") {
    WriteLog "Script completed without error"
    exit 0
} else {
    WriteLog "Script completed with error(s):"
    WriteLog $error_msg
    exit 1
}