################################################################################################################
#
# PernixData FVP - Get FVP Management Server
# Author: Andy Daniel
# Created: 07/15/2015
#          
# Notes: This script will return the name of the FVP Management Server associated with a vCenter Server.
#        
# Usage: Run this script on a host that has VMware PowerCLI installed.
#       
################################################################################################################
$report = @()
$row = "" | Select "vCenter Server","FVP Management Server"
$vCenter = Read-Host "Enter vCenter Server name"
Connect-VIServer $vCenter | Out-Null
$ExtensionManager = Get-View ExtensionManager
$fvp_server = $ExtensionManager.ExtensionList | Select-Object Key,Version,@{Name='URL';Expression={$_.Server.url}} | Where-Object {$_.Key -eq "com.pernixdata.mgmtserver"}
if ($fvp_server){
	$row."vCenter Server" = $vCenter
	$row."FVP Management Server" = $fvp_server.URL.Substring(8,($fvp_server.URL.LastIndexOf(":")-8))
	$report += $row
	$report | ft
} else {
	Write-Host "PernixData FVP Management Server associated with $vCenter not found."
} 